<?php
    // Starting session
    session_start();

    if (!isset($_SESSION['email']) ){
        header('location: index.php?login=access_denied');
    }

    $page = 'about';

    $title = 'About Page';
    include_once 'includes/head.php';
    include_once 'includes/navbar.php';
?>
        
    <div class="container">
        <div class="col">
            <h3 class="my-4">About Us</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima suscipit beatae pariatur dolorem aliquid quidem nesciunt mollitia maxime deleniti, fugiat qui provident nihil, officiis minus illum! Exercitationem laudantium recusandae maiores.</p>
        </div>
    </div>
        
<?php
    include_once 'includes/script.php';
?>