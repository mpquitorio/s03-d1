<?php
    // Starting session
    session_start();

    if (!isset($_SESSION['email']) ){
        header('location: index.php?login=access_denied');
    }

    if( isset( $_GET['message'] ) ){

        $response = $_GET['message'];

        if( $response == 'success' ){
            $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Successfully Added!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if( $response == 'failed' ){
            $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Connection Failed!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

    }else {
        $response = "";
    }

    $title = 'Add Album Page';
    include_once 'includes/head.php';
    include_once 'includes/navbar.php';
?>
        
    <div class="container">
        <div class="col">
            <h3 class="my-4">Add Albums</h3>
            <form method="POST" action="includes/add_album_action.php">
                <?php
                    echo $response; 
                ?>
                <div class="form-group">
                    <select class="form-control" name="artist_id">
                        <?php 
                            include_once 'includes/db_connect.php';

                            $sql = "SELECT * FROM `artists`";
                            $result = mysqli_query($conn, $sql);

                            if( mysqli_num_rows($result) > 0 ){
                                while( $row = mysqli_fetch_assoc($result) ){
                                    echo "<option value='$row[artist_id]'>
                                             $row[artist_name]
                                          </option>";
                                 }
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="album_name" placeholder="Enter Album Name" required> 
                </div>
                <div class="form-group"> 
                    <input type="text" class="form-control" name="album_year" placeholder="Enter Album Year" required> 
                </div>
                <input type="submit" value="Submit" class="btn btn-primary">
            </form>
        </div>
    </div>
        
<?php
    include_once 'includes/script.php';
?>