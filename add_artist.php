<?php
    // Starting session
    session_start();

    if (!isset($_SESSION['email']) ){
        header('location: index.php?login=access_denied');
    }

    if( isset( $_GET['message'] ) ){

        $response = $_GET['message'];

        if( $response == 'success' ){
            $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Successfully Added!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if( $response == 'failed' ){
            $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Connection Failed!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

    }else {
        $response = "";
    }

    $title = 'Add Artist Page';
    include_once 'includes/head.php';
    include_once 'includes/navbar.php';
?>
        
    <div class="container">
        <div class="col">
            <h3 class="my-4">Add Artist</h3>
            <?php 
                echo $response; 
            ?>
            <form method="POST" action="includes/add_artist_action.php">
                <div class="form-group">
                    <input type="text" class="form-control" name="artist" placeholder="Enter Artist Name" required> 
                </div>
                <input type="submit" value="Submit" class="btn btn-primary">
            </form>
        </div>
    </div>
        
<?php
    include_once 'includes/script.php';
?>