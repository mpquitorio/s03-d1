<?php
    // Starting session
    session_start();

    if (!isset($_SESSION['email']) ){
        header('location: ../index.php?login=access_denied');
    }

    if( isset( $_GET['message'] ) ){

        $response = $_GET['message'];

        if( $response == 'success' ){
            $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Successfully Added!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if( $response == 'failed' ){
            $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Connection Failed!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

    }else {
        $response = "";
    }

    $title = 'Edit Artist Page';
    include_once 'includes/head.php';
    include_once 'includes/navbar.php';
?>
        
    <div class="container">
        <div class="col">
            <h3 class="my-4">Edit Record</h3>
            <?php
                $artist_id = $_GET['artist_id'];

                include_once 'includes/db_connect.php';
                
                $sql = "SELECT * FROM `artists` WHERE artist_id='$artist_id'";
                $result = mysqli_query($conn, $sql);

                while ($row = mysqli_fetch_assoc($result)){
                    $artist_name = $row['artist_name'];
                }
            ?>
            <form method="POST" action="includes/edit_record_action.php">
                <div class="form-group">
                    <input type="hidden" name="artist_id" value="<?php echo $artist_id; ?>">
                    <input type="text" class="form-control" name="artist_name" value="<?php echo $artist_name; ?>" > 
                </div>
                <input type="submit" value="Update" class="btn btn-primary">
            </form>
        </div>
    </div>
        
<?php
    include_once 'includes/script.php';
?>