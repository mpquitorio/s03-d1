        <!-- JS -->
        <script src="./assets/js/jquery-3.4.1.slim.min.js"></script>
        <script src="./assets/js/popper.min.js"></script>
        <script src="./assets/js/bootstrap-4.4.1/bootstrap.min.js"></script>
        <script src="./assets/js/script.js"></script>
    
        <script type="text/javascript">
            $(document).ready(function() {

                let qty   = $('.qty');
                let price = $('.price');
                let sub   = $('.sub');
                let total = $('.total'); 

                for (let i = 0; i < qty.length; i++){
                    $(qty[i]).on("change", function() {
                        let subTotal = parseFloat( $(qty[i]).val() * $(price[i]).html() );
                        $(sub[i]).html( subTotal.toFixed(2) );
                        getTotal();
                    });
                }

                function getTotal(){
                    let sum = 0;
                    $('tbody > tr').each(function () {
                        sum += parseFloat( $(this).children().children('.sub').html() );
                    });
                    $(total).html( sum.toFixed(2) );
                }

                $('.remove').on("click", function() {
                    $(this).parent().parent().remove();
                    getTotal();
                    if( $('.qty').length == 0 ) {
                        $('.result').html("Empty Cart");
                    }
                });

            });
        </script>
    </body>
    <!-- end::Body -->
</html>