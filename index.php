<?php 
    // isset() function checks whether a variable is set and is not null.
    if( isset( $_GET['login'] ) ){

        $response = $_GET['login'];

        if ( $response == "failed" ){
            $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Invalid Credentials!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if ( $response == "access_denied" ){
            $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Access Denied!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if ( $response == "logout_success" ){
            $response = "<div class='alert alert-warning alert-dismissible fade show' role='alert'>You've been logged out!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

    }else{
        $response = "";
    }

    $title = 'PHP Programming';
    include_once 'includes/head.php';
?>

    <div class="container p-5">
        <div class="col">
            <h1>Login Form</h1>
            <?php echo $response; ?>
            
            <form action="includes/index_action.php" method="POST">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" name="email" id="email" placeholder="Ex: root@localhost.com" required> 
                    <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" name="pwd" id="pwd" required> 
                </div>
                <input type="submit" value="Submit" class="btn btn-primary">
                <input type="reset" value="Clear" class="btn btn-primary">
            </form>
        </div>
    </div>
        
<?php
    include_once 'includes/script.php';
?>