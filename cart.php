<?php
    // Starting session
    session_start();

    if (!isset($_SESSION['email']) ){
        header('location: index.php?login=access_denied');
    }

    $page = 'cart';

    $title = 'My Cart';
    include_once 'includes/head.php';
    include_once 'includes/navbar.php';
?>
        
    <div class="container">
        <div class="col">
            <h3 class="my-4">About Us</h3>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Item</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price</th>
                            <th scope="col">Sub-Total</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Watch</td>
                            <td><input type="number" value="0" min="0" max="10" class="qty pl-1"></td>
                            <td>P&nbsp;<span class="price">200.50</span></td>
                            <td>P&nbsp;<span class="sub">0.00</span></td>
                            <td class="position-relative"><button class="position-absolute remove">Remove</button></td>
                        </tr>
                        <tr>
                            <td>Hat</td>
                            <td><input type="number" value="0" min="0" max="10" class="qty pl-1"></td>
                            <td>P&nbsp;<span class="price">35.50</span></td>
                            <td>P&nbsp;<span class="sub">0.00</span></td>
                            <td class="position-relative"><button class="position-absolute remove">Remove</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr>
            <h4 class="ml-auto result">Total: P&nbsp;<span class="total">0.00</span></h4>
        </div>
    </div>   
<?php
    include_once 'includes/script.php';
?>