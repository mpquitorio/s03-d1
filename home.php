<?php
    // Starting session
    session_start();

    if (!isset($_SESSION['email']) ){
        header('location: index.php?login=access_denied');
    }

    $page = 'home';

    $title = 'Home Page';
    include_once 'includes/head.php';
    include_once 'includes/navbar.php';
?>
        
    <div class="container">
        <div class="col">
            <h3 class="my-4">Welcome <?php echo $_SESSION["email"] ?>!</h3>
        </div>
    </div>
        
<?php
    include_once 'includes/script.php';
?>