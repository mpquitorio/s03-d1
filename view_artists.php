<?php
    // Starting session
    session_start();

    if (!isset($_SESSION['email']) ){
        header('location: index.php?login=access_denied');
    }

    if( isset( $_GET["message"] ) ){

        $response = $_GET["message"];

        if( $response == 'success' ){
            $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Successfully Deleted!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if( $response == 'failed' ){
            $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Connection Failed!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if( $response == 'updated' ){
            $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Successfully Updated!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

    }else {
        $response = "";
    }

    $title = 'View Artist Page';
    include_once 'includes/head.php';
    include_once 'includes/navbar.php';
?>
        
    <div class="container">
        <div class="col">
            <h3 class="my-4">View Artists</h3>
            <?php
                echo $response; 

                include_once 'includes/db_connect.php';
                
                $sql = "SELECT * FROM `artists`";

                $result = mysqli_query($conn, $sql);

                echo '<table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Artist Name</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>';

                if( mysqli_num_rows($result) > 0 ){
                    
                    $count = 1; 
                    while( $row = mysqli_fetch_assoc($result) ){

                        echo '<tr>
                                <td>' . $count .'</td>
                                <td>' . $row['artist_name'] .'</td>
                                <td><a class="text-decoration-none" href="edit_record.php?artist_id=' . $row['artist_id'] . '">Edit</a> | <a class="text-decoration-none" href="includes/delete_record.php?artist_id=' . $row['artist_id'] . '">Delete</a></td>
                            </tr>';

                        $count++;
                    }

                    echo '</tbody></table>';
                }else{
                    echo "No Records Found";
                }
            ?>
        </div>
    </div>
        
<?php
    include_once 'includes/script.php';
?>